<!-- sử dụng tiếng việt -->
<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8"%>


<!-- Sidebar menu-->
<div class="app-sidebar__overlay" data-toggle="sidebar"></div>
<aside class="app-sidebar">
    <ul class="app-menu">
        <li><a class="app-menu__item" href="${base}/admin/report"><i class="app-menu__icon fa fa-file-text"></i><span class="app-menu__label">Báo cáo thông kê</span></a></li>
        <li><a class="app-menu__item" href="${base}/admin/list-product"><i class="app-menu__icon fa fa-dashboard"></i><span class="app-menu__label">Quản lý sản phẩm</span></a></li>
        <li><a class="app-menu__item" href="${base}/admin/list-order"><i class="app-menu__icon fa fa-truck"></i><span class="app-menu__label">Quản lý Đơn hàng</span></a></li>
        <li><a class="app-menu__item" href="${base}/admin/contacts"><i class="app-menu__icon fa fa-envelope"></i><span class="app-menu__label">Quản lý liên hệ</span></a></li>
        <li><a class="app-menu__item" href="${base}/admin/users"><i class="app-menu__icon fa fa-users"></i><span class="app-menu__label">Quản lý tài khoản</span></a></li>
        <li><a class="app-menu__item" href="${base}/admin/list-role"><i class="app-menu__icon fa fa-user"></i><span class="app-menu__label">Phân quyền</span></a></li>
        <li><a class="app-menu__item" href="${base}/admin/list-category"><i class="app-menu__icon fa fa-file-text"></i><span class="app-menu__label">Quản lý thương hiệu</span></a></li>
        <li><a class="app-menu__item" href="${base}/admin/sales"><i class="app-menu__icon fa fa-file-text"></i><span class="app-menu__label">Quản lý mã giảm giá</span></a></li>
    </ul>
</aside>