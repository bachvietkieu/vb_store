package com.portal.controller.users;

import com.portal.entities.Product;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;

@Controller
public class ShopController {
    RestTemplate restTemplate = new RestTemplate();

    @RequestMapping(value = "/shop", method = RequestMethod.GET)
    public String producs(Model model){
        ResponseEntity<Product[]> products = restTemplate.getForEntity("http://localhost:8081/admin/list-product", Product[].class);
        model.addAttribute("listProducts", products.getBody());
        return "users/shop";
    }
}
